package all.strings;

public class StringPractice {
    public static void main(String[] args) {

        String hello = "xhello";
        System.out.println("1 - " + hello.length());

        String extractedString = hello.substring(1);
        System.out.println("2 - rest : " + extractedString);

        hello = "xxxxhello";
        extractedString = hello.substring(1, 4);
        System.out.println("3 - cut : " + extractedString);

        String c = "fun";
        String d = "there";

        if (c == "fun") {
            // This is a trap don't do this
        }

        if (!c.equals(hello)) {
            // Use this
        }

        if (d.equalsIgnoreCase("ThErE")) {
            System.out.println("4 - Print there");
        }

        System.out.println("5 - charAt: " + d.charAt(3));

        char f = 'u';
        System.out.println("6 - index of char : " + c.indexOf(f));
        String str = "ciao Hello there yogi, ciao";
        System.out.println("7 - index of string : " + str.indexOf("yo"));
        System.out.println("8 - index of string start at some position " + str.indexOf("ciao", 5));
        System.out.println("10 - first index of letter : " + str.indexOf("o"));
        System.out.println("9 - last index of : " + str.lastIndexOf("o"));
    }
}

package all;

import java.util.ArrayList;
import java.util.List;

public class FinalVariables {

    private static final int SIZE = 10;

    private static final List<String> VALUES = new ArrayList<>();

    public static void main(String[] args) {
        int[] myArray = new int[SIZE];

        int doubleSize = 2 * SIZE;

        for (int i = 0; i < SIZE; i++) {
            System.out.println(myArray[i] + " " + i);
        }

        VALUES.add("changed");

        final int myNumber = 10;

        System.out.println("VALUES size " + VALUES.size());
        System.out.println(VALUES.get(0));
    }
}

package all;

public class OperatorsChallenge {
    public static void main(String[] args) {
        double doubleVal = 20.00;
        double doubleVal2 = 80.00;
        double multiplier = 100.00;
        double result = (doubleVal + doubleVal2) * multiplier;
        System.out.println(result);

        double reminder3 = result % 3;
        double reminder40 = result % 40.00;
        System.out.println(reminder3);
        System.out.println(reminder40);

        boolean isZero = reminder3 == 0;
        System.out.println(isZero);
    }
}

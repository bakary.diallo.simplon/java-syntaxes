package all;

import all.exceptions.CustomException;

public class CatchErrorByCustomException {
    public static void main(String[] args) throws Exception{

        CustomException myCustomException = new CustomException();

        try {
            System.out.println(myCustomException.substract10FromLargeNumber(9));
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }
}

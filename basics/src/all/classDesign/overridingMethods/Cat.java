package all.classDesign.overridingMethods;

/**
 * @author goran on 14/07/2017.
 */
public class Cat extends Animal {

    public Cat(int age) {
        super(age);
    }
}

package all.classDesign.defaultInterfaceMethods;

public interface Carnivore {

    default void eatMeat() {
        System.out.println("Eating meat");
    }
}

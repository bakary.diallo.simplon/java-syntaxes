package all.classDesign.defaultInterfaceMethods;

public interface Omnivore extends Herbivore, Carnivore {
    void eatPlants();
    void eatMeat();
}

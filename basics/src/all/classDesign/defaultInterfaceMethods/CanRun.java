package all.classDesign.defaultInterfaceMethods;

public interface CanRun {

    void run(int speed);
}

package all.classDesign.inheritance2.main;


import all.classDesign.inheritance2.Dog;
import all.classDesign.inheritance2.Husky;

/**
 * @author goran on 14/07/2017.
 */
public class Main {

    public static void main(String[] args) {

        Husky husky = new Husky(5);
        husky.printDetails();

        Dog dog = new Dog(3);
        dog.setName("Rex");
        dog.printDetails();
    }
}

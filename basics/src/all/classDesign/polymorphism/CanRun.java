package all.classDesign.polymorphism;

/**
 * @author goran on 15/07/2017.
 */
public interface CanRun {

    void run(int speed);
}

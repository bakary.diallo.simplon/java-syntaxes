package all.classDesign.abstractClass.main;
import all.classDesign.abstractClass.Rabbit;
import all.classDesign.abstractClass.Dog;
import all.classDesign.abstractClass.Husky;

public class Main {

    public static void main(String[] args) {

        Husky husky = new Husky(5);
        husky.printDetails();

        Dog dog = new Dog(3);
        dog.setName("Rex");
        dog.printDetails();
        System.out.println("age " + dog.getAge());

        husky.eat();
        System.out.println("avg= " + husky.getAverageWeight());

        Rabbit rabbit = new Rabbit();
        Rabbit rabbit1 = new Rabbit(2);
    }
}

package all.classDesign.abstractClass;

public class Rabbit extends Animal {

    public Rabbit() {
        super();
    }

    public Rabbit(int age) {
        super(3);
    }
}

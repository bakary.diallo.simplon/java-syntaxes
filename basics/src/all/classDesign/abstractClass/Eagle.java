package all.classDesign.abstractClass;

public class Eagle extends Bird {

    public int fly(int height) {
        System.out.println("Eagle is flying at " + height + " meters");
        return height;
    }

    @Override
    public void eat(int amount) {
        System.out.println(amount);
    }
}

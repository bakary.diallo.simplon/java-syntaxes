package all.javaTypes;

public class BooleanCharFloatDouble {
    public static void main(String[] args) {
        boolean isTrue = true;
        boolean isFalse = false;
        char newChar = 'A';
        float floatMinValue = Float.MIN_VALUE;
        float floatMaxValue = Float.MAX_VALUE;
        double doubleMinValue = Double.MIN_VALUE;
        double doubleMaxValue = Double.MAX_VALUE;
        int myMaxIntTest = 2_147_483_647;

        float myFloatVal = 5.25f;
        double myDoubleVal = 5.26d;

        System.out.print(
                "isTrue" + isTrue + "\n" +
                        "isFalse : " + isFalse + "\n" +
                        "newChar : " + newChar + "\n" +
                        "floatMinValue : " + floatMinValue + "\n" +
                        "floatMaxValue : " + floatMaxValue + "\n" +
                        "doubleMinValue : " + doubleMinValue + "\n" +
                        "doubleMaxValue : " + doubleMaxValue + "\n" +
                        "myMaxIntTest : " + myMaxIntTest
        );
    }
}

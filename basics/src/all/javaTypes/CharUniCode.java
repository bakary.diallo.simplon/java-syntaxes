package all.javaTypes;

/**
 * Unicode chat table
 * https://unicode-table.com/en/#latin-extended-b
 */
public class CharUniCode {
    public static void main(String[] args) {
        char myChar = 'D';
        char myUnicodeChar = '\u0044';
        System.out.println(myChar);
        System.out.println(myUnicodeChar);
        char myCopyrightChar = '\u00A9';
        System.out.println(myCopyrightChar);
    }
}

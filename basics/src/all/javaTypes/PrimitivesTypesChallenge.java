package all.javaTypes;

public class PrimitivesTypesChallenge {

    public static void test() {
        byte byteValue = 10;
        short shortValue = 20;
        int intValue = 50;

        long longTotal = 50000L + 10L * (byteValue + shortValue + intValue);
        System.out.println("Total : " + longTotal);

        short shortTotal = (short) (1000 + 10 * (byteValue + shortValue + intValue));
        System.out.println("ShortTotal : " + shortTotal);
    }

    public static void main(String[] args) {
        byte byteValue = 127;
        short shortValue = 32767;
        int intNumber = 2147483647;
        long longResult = 50000 + 10L * ((byteValue + shortValue + intNumber));
        System.out.println("Long result : " + longResult);
        test();
    }
}

package all.javaTypes;

public class ByteShortIntLong {

    public static void main(String[] args) {
        byte byteMinValue = Byte.MIN_VALUE;
        byte byteMaxValue = Byte.MAX_VALUE;
        short shortMinValue = Short.MIN_VALUE;
        short shortMaxValue = Short.MAX_VALUE;
        int intMinValue = Integer.MIN_VALUE;
        int intMaxValue = Integer.MAX_VALUE;
        long longMinValue = Long.MIN_VALUE;
        long longMaxValue = Long.MAX_VALUE;
        long bigLongLiteralValue = 2_147_483_647_234L;
        System.out.print(
                "byteMinValue : " + byteMinValue + "\n" +
                        "byteMaxValue : " + byteMaxValue + "\n" +
                        "shortMinValue : " + shortMinValue + "\n" +
                        "shortMaxValue : " + shortMaxValue + "\n" +
                        "intMinValue : " + intMinValue + "\n" +
                        "intMaxValue : " + intMaxValue + "\n" +
                        "longMinValue : " + longMinValue + "\n" +
                        "longMaxValue : " + longMaxValue + "\n" +
                        "bigLongLiteralValue : " + bigLongLiteralValue
        );
    }
}

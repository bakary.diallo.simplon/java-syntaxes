package all;

import java.util.Random;

public class RandomValues {
    public static void main(String[] args) {
        Random random = new Random();
        System.out.println("random = " + random.nextInt());
        System.out.println("random = " + random.nextFloat());
        System.out.println("random = " + random.nextLong());
        System.out.println("random = " + random.nextDouble());
    }
}

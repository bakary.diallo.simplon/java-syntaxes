package all;

import java.util.Arrays;

public class VariableArguments {

    public static void testMain(String[] args) {
        System.out.println("args length " + args.length);
        System.out.println(Arrays.toString(args));
    }

    private static void print(String... myArguments) {
        System.out.println(Arrays.toString(myArguments));
    }

    public static void main(String... args) {
        System.out.println(args.length);
        print(args);
        print("Java");
        print("this", "is", "my", "string", "array");
        print("variable", "arguments", "are", "nice", "and", "useful", "feature");
        String[] s = new String[]{"toto", "arguments", "are", "nice", "and", "useful", "feature"};
        testMain(s);
    }


}

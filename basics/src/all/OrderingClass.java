package all;

class MyOtherClass {
}

class AnotherClass {
}

public class OrderingClass {
    static int myInt = 10;

    public static void myMethod(int a) {
        System.out.println("anotherInt " + a);
    }

    public static void main() {
        int anotherInt = myInt;
        System.out.println("myInt " + myInt);
        myMethod(myInt);
    }
}

class RenderOrderingClass {
    public static void main(String[] args) {
        OrderingClass.main();
    }
}

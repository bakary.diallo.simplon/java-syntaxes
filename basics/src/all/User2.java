package all;

import java.util.Objects;

public class User2 {
    private String name;
    private int age;
    int salary;

    public User2() {

    }

    public User2(int salary, String name) {
        this.name = name;
        this.salary = salary;
    }

    public User2(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User2)) return false;
        User2 user = (User2) o;
        return getAge() == user.getAge() && getName().equals(user.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAge());
    }

    public void raise(int raise) {
        setSalary(this.getSalary() + raise);
    }
}

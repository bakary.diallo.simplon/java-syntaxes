package all;

import java.io.*;

public class ReadFile {
    public static void main(String[] args) throws IOException {
        File file = new File("/Users/bakarydiallo/Desktop/javaPath2/3-java-syntaxes/myfile.txt");

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();

            while (line != null) {
                System.out.println(line);
                // allow to pass to the next line
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Problem reading the file " + file.getName());
        }
    }
}

package all.methods;

public class MethodsClass {
    public static void printSomeJunk(String arg) {
        System.out.println("some " + arg);
    }

    public static void intPassAsArg(int age) {
        System.out.println("My age is " + age);
    }

    public static double discount(int someArg) {
        return (someArg * 0.75);
    }

    public void nonStaticMethod() {
        System.out.println("Need instance of the class ...");
    }

}

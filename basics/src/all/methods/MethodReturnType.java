package all.methods;

/**
 * @author goran on 12/07/2017.
 */
public class MethodReturnType {

    public void jump() {
    }

    public void jump2() {
        // return is redundant
        return;
    }

    public String jump3() {
        return "";
    }

    String jump6(int a) {
        if (a == 5) {
            return "";
        }
        return "abc";
    }

  public static int getInt() {
        return 9;
    }

    int getLong() {
        return (int) 9L;
    }

    int expanded() {
        return 10;
    }

    boolean isTrue() {
        return 5 == 5;
    }

    public void test() {
    }

    public void $test() {
    }

    public void _test() {
    }
}

package all.methods;

import all.User1;

public class MainMethod {
    public static void main(String[] args) {
        User1 u1 = new User1();
        User1 u2 = new User1("john", 6);
        User1 u3 = new User1("john", 6);

        System.out.println("U1 = " + u1);
        System.out.println("U2 = " + u2);
        System.out.println("U2 name = " + u2.getName());

        System.out.println("u2 == u3 " + (u2 == u3));
        System.out.println("u2 equals u3 " + (u2.equals(u3)));

        System.out.println("u1 hashCode = " + u1.hashCode());
        System.out.println("u2 hashCode = " + u2.hashCode());
        System.out.println("u3 hashCode = " + u3.hashCode());

        User1 u4 = new User1 ("lucie", 1000);
        System.out.println("u4 " + u4);
        u4.raise(1200);
        System.out.println("u4 raise salary " + u4.getSalary());
    }
}

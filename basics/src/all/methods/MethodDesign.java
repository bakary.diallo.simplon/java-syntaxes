package all.methods;

/**
 * @author goran on 12/07/2017.
 */
public class MethodDesign {

    public void jump() {
    }

    void jump3() {
    }

    public final void jump4() {
    }

    public static void jump5() {
    }

    public static void jump6() {
    }


    static public void jump10() {
    }

    public void jump11() {
    }
}

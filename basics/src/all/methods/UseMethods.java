package all.methods;

import static all.methods.MethodsClass.discount;

public class UseMethods {
    public static void main(String[] args) {
        MethodsClass.printSomeJunk("bla bla bla");
        MethodsClass.intPassAsArg(37);
        double price = discount(10) * 0.6;
        System.out.println("Price " + price);
        MethodsClass utils = new MethodsClass();
        utils.nonStaticMethod();
    }
}

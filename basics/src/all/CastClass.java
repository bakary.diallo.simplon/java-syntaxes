package all;

public class CastClass {
    public static void main(String[] args) {
        int minValue = Integer.MIN_VALUE;
        System.out.println(minValue);

        byte byteResult = (byte) (minValue/2);
        short shortResult = (short) (minValue);

        System.out.println(byteResult);
        System.out.println(shortResult);
    }
}


package all;

import java.util.Date;

public class NamingConflictDate {

    java.sql.Date sqlDate;
    java.util.Date date;

    public static void main(String[] args) {
        Date date = new java.util.Date();
        long timeInMilliSeconds = date.getTime();
        java.sql.Date date1 = new java.sql.Date(timeInMilliSeconds);

        System.out.println("timeInMilliSeconds " + timeInMilliSeconds);
        System.out.println("SQL Date: " + date1);
    }
}

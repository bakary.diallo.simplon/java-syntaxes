package all.loops;

public class LoopPractice {
    public static void main(String[] args) {
        int count = 0;

        while (count <= 100) {
            System.out.println(" - count : " + count);
            count++;

            if (count == 50) {
                System.out.println("Stop at 50");
                break;
            }
        }
        System.out.println(count);

        String str = "We have a large inventory of things in our warehouse falling in "
                + "the category:apparel and the slightly "
                + "more in demand category:makeup along with the category:furniture and ….";

        printCategories(str);

    }

    /**
     * Extract all categories from the string method
     *
     * @param str
     * @return
     */
    public static void printCategories(String string) {

        int i = 0;

        while (true) {
            int found = string.indexOf("category:", i);
            if (found == -1) break;
            int start = found + 9; // start of the actual category
            int end = string.indexOf(" ", start);
            System.out.println(string.substring(start, end));
            i = end + 1;
        }

    }
}

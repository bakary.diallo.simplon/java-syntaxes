package all;

public class EarthClass {

    public static void main(String[] args) {

        HumanClass tom;
        tom = new HumanClass();
        tom.age = 5;
        tom.eyeColor = "brown";
        tom.heightInInches = 72;
        tom.name = "Tom Zhao";
        tom.speak();

        HumanClass joe;
        joe = new HumanClass();
        joe.age = 5;
        joe.eyeColor = "brown";
        joe.heightInInches = 72;
        joe.name = "Joe Black";
        joe.speak();

        HumanClass catalina = new HumanClass("Catalina", 23, 80, "green");
        catalina.speak();
    }
}

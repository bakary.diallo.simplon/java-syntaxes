package all.staticMethods;
import all.staticMethods.common.Common;

public class UsingAccessModifiers {

    public static void main(String[] args) {
        Common common = new Common();
        common.publicPrint();
        System.out.println("publicNumber= " + common.publicNumber);
    }
}

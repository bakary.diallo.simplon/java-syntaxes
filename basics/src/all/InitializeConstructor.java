package all;

public class InitializeConstructor {
    String name;

    public InitializeConstructor() {
        this.name = "duke";
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        InitializeConstructor initializeConstructor = new InitializeConstructor();
        System.out.println(initializeConstructor.getName());
    }
}

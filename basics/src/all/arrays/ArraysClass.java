package all.arrays;

public class ArraysClass {
    public static void main(String[] args) {
        double[] values = new double[100];
        values[0] = 1000;
        values[1] = 33.44;
        values[99] = 93432;
        System.out.println(values[0] + " _ " + values[1] + " _ " + values[99]);
    }
}

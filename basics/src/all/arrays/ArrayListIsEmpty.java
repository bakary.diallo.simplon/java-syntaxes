package all.arrays;

import java.util.ArrayList;
import java.util.List;

public class ArrayListIsEmpty {

    static ArrayList<Integer> myArr = new ArrayList<>();

    public static void fillArr(List<Integer> arr, int length) {
        for (int i = 0; i < length; i++) {
            arr.add(i);
        }
        System.out.println(arr);
    }

    public static void main(String[] args) {

        fillArr(myArr, 5);

        if (myArr.isEmpty()) {
            System.out.println("fill the arr");
        } else {
            System.out.println(myArr);
        }
    }
}

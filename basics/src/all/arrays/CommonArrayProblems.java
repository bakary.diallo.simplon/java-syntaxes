package all.arrays;

public class CommonArrayProblems {

    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};

        for (int i = 1; i < numbers.length; i++) {
            System.out.println(i + " - " + numbers[i]);
        }

        for (int i = 1; i <= numbers.length - 1; i++) {
            System.out.println(i + " - " + numbers[i]);
        }
        int[] nums = new int[20]; // size only at initialization
        int size = numbers.length;
        System.out.println(nums.length + " " + size);
    }
}

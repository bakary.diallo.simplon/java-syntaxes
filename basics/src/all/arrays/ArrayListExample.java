package all.arrays;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {

    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList<>();
        ArrayList<Object> myList2 = new ArrayList<Object>();

        ArrayList<String> list = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>(); // diamond operator
        List<String> list3 = new ArrayList<>(); // using interface as type

        List<String> list5 = new ArrayList<>();
        myList.add(2);
        myList2.add(2);
        list.add("2");
        list2.add("2");
        list3.add("2");

        for (int i = 0; i < 25; i++) {
            list5.add("z");
        }
        System.out.println(list5.size());
    }
}

package all.abstractClass;

import all.javaInterfaces.Hashtail;

public abstract class Puma implements Hashtail {
    public int getTailLength() {
        return 4;
    }
}

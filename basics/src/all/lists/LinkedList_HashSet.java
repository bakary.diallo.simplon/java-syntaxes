package all.lists;

import all.Vehicle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class LinkedList_HashSet {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<String>();
        words.add("hello");
        words.add("there");
        words.add("10");

        String item1 = words.get(2);
        System.out.println(item1);

        LinkedList<Integer> numbers = new LinkedList<Integer>();
        numbers.add(100432345);
        numbers.add(200);
        numbers.add(45);
        numbers.add(1000);
        numbers.remove();

        HashSet<Integer> preventDuplicates = new HashSet<Integer>();
        preventDuplicates.add(100);
        preventDuplicates.add(200);
        preventDuplicates.add(45);
        preventDuplicates.add(100);

        for (Integer number : numbers) {
            System.out.println(number);
        }

        List<Vehicle> vehicles = new LinkedList<Vehicle>();
        Vehicle vehicle2 = new Vehicle("Toyota", "Camery", 12000, false);
        Vehicle vehicle3 = new Vehicle("Jeep", "Wrangler", 25000, true);

        vehicles.add(new Vehicle("Honda", "Accord", 12000, false));
        vehicles.add(vehicle2);
        vehicles.add(vehicle3);
        vehicles.add(vehicle3);


        for (Vehicle vehicle : vehicles){
            System.out.println(vehicle.toString());
        }

        for (Integer preventDuplicate : preventDuplicates){
            System.out.println(preventDuplicate);
        }
    }
}















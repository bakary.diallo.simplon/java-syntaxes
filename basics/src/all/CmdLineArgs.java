package all;

public class CmdLineArgs {
    public static void main(String[] args) {

        args = new String[]{"Parrot", "Dog", "Cat"};

        System.out.println("args-size " + args.length);

        for (int i = 0; i < args.length; i++) {
            System.out.println("args[" + i + "]=" + args[i]);
        }

        System.out.println(sum(2, 2));
    }

    public static int sum(int a, int b) {
        return a + b;
    }
}

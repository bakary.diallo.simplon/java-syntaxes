package all.algos.array.utils;

import java.util.ArrayList;

public class SoutArrayList {
    static ArrayList<Integer> nums = new ArrayList<>();

    public static void main(String[] args) {
        nums.add(1);
        nums.add(2);
        nums.add(3);

        for (Integer num : nums) {
            System.out.println(num);
        }
    }
}

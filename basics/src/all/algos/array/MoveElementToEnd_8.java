package all.algos.array;

import all.algos.array.utils.FillArr;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * You're given an array of integers and an integer. Write a function that moves
 * all instances of that integer in the array to the end of the array and returns
 * the array.
 * </p>
 * <p>
 * The function should perform this in place (i.e., it should mutate the input
 * array) and doesn't need to maintain the order of the other integers.
 * </p>
 * <h3>Sample Input</h3>
 * <pre><span class="CodeEditor-promptParameter">array</span> = [2, 1, 2, 2, 2, 3, 4, 2]
 * <span class="CodeEditor-promptParameter">toMove</span> = 2
 * </pre>
 * <h3>Sample Output</h3>
 * <pre>[1, 3, 4, 2, 2, 2, 2, 2] <span class="CodeEditor-promptComment">// the numbers 1, 3, and 4 could be ordered differently</span>
 * </pre>
 */
public class MoveElementToEnd_8 {

    static int[] arr = new int[]{2, 1, 2, 2, 2, 3, 4, 2};
    static ArrayList<Integer> myArray = new ArrayList<>();
    static int toMove = 2;

    public static List<Integer> moveElementToEnd(List<Integer> array, int toMove) {
        int i = 0;
        int j = array.size() - 1;
        while (i < j) {
            while (i < j && array.get(j) == toMove) j--;
            if (array.get(i) == toMove) swap(i, j, array);
            i++;
        }
        return array;
    }

    public static void swap(int i, int j, List<Integer> array) {
        int temp = array.get(j);
        array.set(j, array.get(i));
        array.set(i, temp);
    }

    public static void main(String[] args) {
        FillArr.fillArrayList(myArray, arr);
        System.out.println(moveElementToEnd(myArray, toMove));
    }
}


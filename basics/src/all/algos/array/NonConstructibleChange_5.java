package all.algos.array;

import java.util.Arrays;

/**
 * <p>
 * Given an array of positive integers representing the values of coins in your
 * possession, write a function that returns the minimum amount of change (the
 * minimum sum of money) that you <b>cannot</b> create. The given coins can have
 * any positive integer value and aren't necessarily unique (i.e., you can have
 * multiple coins of the same value).
 * </p>
 * <p>
 * For example, if you're given <span>coins = [1, 2, 5]</span>, the minimum
 * amount of change that you can't create is <span>4</span>. If you're given no
 * coins, the minimum amount of change that you can't create is <span>1</span>.
 * </p>
 * <h3>Sample Input</h3>
 * <pre><span class="CodeEditor-promptParameter">coins</span> = [5, 7, 1, 1, 2, 3, 22]
 * </pre>
 * <h3>Sample Output</h3>
 * <pre>20
 * </pre>
 */
public class NonConstructibleChange_5 {

    static int[] arr = new int[]{5, 7, 1, 1, 2, 3, 22};
    static int[] arr2 = new int[]{1, 5, 1, 1, 1, 10, 15, 20, 100};

    public static int nonConstructibleChange(int[] coins) {
        Arrays.sort(coins);

        int currentChangeCreated = 0;

        for (int coin : coins) {
            if (coin > currentChangeCreated + 1) {
                return currentChangeCreated + 1;
            }
            currentChangeCreated += coin;
        }
        return currentChangeCreated;
    }

    public static void main(String[] args) {
        System.out.println(nonConstructibleChange(arr));
        System.out.println(nonConstructibleChange(arr2));
    }
}

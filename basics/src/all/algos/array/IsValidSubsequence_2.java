package all.algos.array;

import all.algos.array.utils.FillArr;

import java.util.ArrayList;
import java.util.List;

public class IsValidSubsequence_2 {

    /**
     * <p>
     *   Given two non-empty arrays of integers, write a function that determines
     *   whether the second array is a subsequence of the first one.
     * </p>
     * <p>
     *   A subsequence of an array is a set of numbers that aren't necessarily adjacent
     *   in the array but that are in the same order as they appear in the array. For
     *   instance, the numbers <span>[1, 3, 4]</span> form a subsequence of the array
     *   <span>[1, 2, 3, 4]</span>, and so do the numbers <span>[2, 4]</span>. Note
     *   that a single number in an array and the array itself are both valid
     *   subsequences of the array.
     * </p>
     * <h3>Sample Input</h3>
     * <pre><span class="CodeEditor-promptParameter">array</span> = [5, 1, 22, 25, 6, -1, 8, 10]
     * <span class="CodeEditor-promptParameter">sequence</span> = [1, 6, -1, 10]
     * </pre>
     * <h3>Sample Output</h3>
     * <pre>true
     * </pre>
     * @param array
     * @param sequence
     * @return
     */
    public static boolean method(List<Integer> array, List<Integer> sequence) {
        int arrIdx = 0;
        int seqIdx = 0;

        while (arrIdx < array.size() && seqIdx < sequence.size()) {
            if (array.get(arrIdx).equals(sequence.get(seqIdx))) {
                seqIdx++;
            }
            arrIdx++;
        }
        return seqIdx == sequence.size();
    }

    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        ArrayList<Integer> seq = new ArrayList<>();
        int[] arrValues = new int[]{5, 1, 22, 25, 6, -1, 8, 10};
        int[] seqValues = new int[]{1, 6, -1, 10};
        FillArr.fillArrayList(arr, arrValues);
        FillArr.fillArrayList(seq, seqValues);
        System.out.println(method(arr, seq));
    }
}

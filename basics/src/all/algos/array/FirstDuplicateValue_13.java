package all.algos.array;

/**
 * <pre>
 *     Given an array of integers between 1 and n, inclusive, where n is the length of the array, write a function that returns the first integer that appears more than once (when the array is read from left to right).
 *
 * In other words, out of all the integers that might occur more than once in the input array, your function should return the one whose first duplicate value has the minimum index.
 *
 * If no integer appears more than once, your function should return -1.
 *
 * Note that you're allowed to mutate the input array.
 *
 * Input array = [2, 1, 5, 2, 3, 3, 4]
 * Output 2
 *
 * Input array = [2, 1, 5, 3, 3, 2, 4]
 * Output 3
 * </pre>
 */
public class FirstDuplicateValue_13 {
    public static int firstDuplicateValue(int[] array) {
        int minimumSecondIndex = array.length;
        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            for (int j = i + 1; j < array.length; j++) {
                int valueToCompare = array[j];
                if (value == valueToCompare) {
                    minimumSecondIndex = Math.min(minimumSecondIndex, j);
                }
            }
        }
        if (minimumSecondIndex == array.length) {
            return -1;
        }
        return array[minimumSecondIndex];
    }

    public static void main(String[] args) {
        System.out.println(
                firstDuplicateValue(new int[]{2, 1, 5, 2, 3, 3, 4})
        );
    }
}

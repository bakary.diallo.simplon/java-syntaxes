package all.algos.array;

import java.util.Arrays;

/**
 * <pre>
 Write a function that takes in a non-empty array of integers and returns an array of the same length,
 where each element in the output array is equal to the product of every other number in the input array.
 In other words, the value at output[i] is equal to the product of every number in the input
 array other than input[i] .
 Note that you're expected to solve this problem without using division.
 Sample Input
 array = [5, 1, 4, 2]
 Sample Output
 [8, 40, 10, 20]
 * </pre>
 */
public class FunctionSum_12 {

    public static int[] arrayOfProducts(int[] array) {
        int[] products = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int runningProduct = 1;
            for (int j = 0; j < array.length; j++) {
                if (i != j) {
                    runningProduct *= array[j];
                }
                products[i] = runningProduct;
            }
        }
        return products;
    }

    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(arrayOfProducts(new int[]{5, 1, 4, 2}))
        );
    }
}

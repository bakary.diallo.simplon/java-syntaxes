package all.algos.array;

import all.algos.array.utils.FillArr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortedSquaredArrayClass_3 {

    /**
     *
     Write a function that takes in a non-empty array of integers that are sorted
     in ascending order and returns a new array of the same length with the squares
     of the original integers also sorted in ascending order.

     * @param array
     * @return
     */
    public static List<Integer> sortedSquaredArray(List<Integer> array) {

        for (int i = 0; i < array.size(); i++) {
            int test = array.get(i) * array.get(i);
            array.set(i, test);
        }
        Collections.sort(array);
        return array;
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        int[] arrayListValues = new int[]{10, 11, 12, 9, 8, 7, 6, 8, 1, 2, 3, 5, 6, 8, 9};
        FillArr.fillArrayList(arrayList, arrayListValues);
        System.out.println(sortedSquaredArray(arrayList));
    }
}

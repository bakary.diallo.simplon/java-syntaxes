package all.algos.array;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Given an array of pairs representing the teams that have competed against each
 * other and an array containing the results of each competition, write a
 * function that returns the winner of the tournament. The input arrays are named
 * <span>competitions</span> and <span>results</span>, respectively. The
 * <span>competitions</span> array has elements in the form of
 * <span>[homeTeam, awayTeam]</span>, where each team is a string of at most 30
 * characters representing the name of the team. The <span>results</span> array
 * contains information about the winner of each corresponding competition in the
 * <span>competitions</span> array. Specifically, <span>results[i]</span> denotes
 * the winner of <span>competitions[i]</span>, where a <span>1</span> in the
 * <span>results</span> array means that the home team in the corresponding
 * competition won and a <span>0</span> means that the away team won.
 */
public class TournamentWinner_4 {

    private static int HOME_TEAM_WON = 1;


    private static String tournamentWinner(
            ArrayList<ArrayList<String>> competitions,
            ArrayList<Integer> results
    ) {
        String currentBestTeam = "";
        HashMap<String, Integer> scores = new HashMap<String, Integer>();
        scores.put(currentBestTeam, 0);

        for (int idx = 0; idx < competitions.size(); idx++) {
            ArrayList<String> competition = competitions.get(idx);
            int result = results.get(idx);

            String homeTeam = competition.get(0);
            String awayTeam = competition.get(1);

            String winningTeam = (result == HOME_TEAM_WON )? homeTeam : awayTeam;

            updateScores(winningTeam, 3, scores);

            if (scores.get(winningTeam) > scores.get(currentBestTeam)) {
                currentBestTeam = winningTeam;
            }
        }
        return currentBestTeam;
    }

    public static void updateScores(String team, int points, HashMap<String, Integer> scores) {
        if (!scores.containsKey(team)) {
            scores.put(team, 0);
        }

        scores.put(team, scores.get(team) + points);
    }

    public static void main(String[] args) {
        ArrayList<Integer> resultsDatas = new ArrayList<>();

        ArrayList<ArrayList<String>> mainArr = new ArrayList<ArrayList<String>>();
        ArrayList<String> subArr = new ArrayList<>();
        ArrayList<String> subArr2 = new ArrayList<>();
        ArrayList<String> subArr3 = new ArrayList<>();
        ArrayList<String> subArr4 = new ArrayList<>();
        ArrayList<String> subArr5 = new ArrayList<>();
        ArrayList<String> subArr6 = new ArrayList<>();
        subArr.add("HTML");
        subArr.add("Java");

        subArr2.add("Java");
        subArr2.add("Python");

        subArr3.add("Python");
        subArr3.add("HTML");

        subArr4.add("C#");
        subArr4.add("Python");

        subArr5.add("Java");
        subArr5.add("C#");

        subArr6.add("C#");
        subArr6.add("HTML");
        mainArr.add(subArr);
        mainArr.add(subArr2);
        mainArr.add(subArr3);
        mainArr.add(subArr4);
        mainArr.add(subArr5);
        mainArr.add(subArr6);

        // 0, 1, 1, 1, 0, 1
        resultsDatas.add(0);
        resultsDatas.add(1);
        resultsDatas.add(1);
        resultsDatas.add(1);
        resultsDatas.add(0);
        resultsDatas.add(1);

        System.out.println(tournamentWinner(mainArr,resultsDatas));

    }
}

package all.algos.binarySearchTrees;

/**
 * * Write a function that takes in a Binary Search Tree (BST) and a target integer
 * value and returns the closest value to that target value contained in the BST.
 * You can assume that there will only be one closest value.
 * <p>
 * Each  BST  node has an integer  value , a
 * left  child node, and a  right  child node. A node is
 * said to be a valid  BST  node if and only if it satisfies the BST
 * property: its  value  is strictly greater than the values of every
 * node to its left; its  value  is less than or equal to the values
 * of every node to its right; and its children nodes are either valid
 * BST  nodes themselves or  None  /  null .
 *
 * <pre>
 *     <pre><span class="CodeEditor-promptParameter">tree</span> =   10
 *        /     \
 *       5      15
 *     /   \   /   \
 *    2     5 13   22
 *  /           \
 * 1            14
 * <span class="CodeEditor-promptParameter">target</span> = 12
 * </pre>
 * </pre>
 */
public class findClosestValue {

    public static int findClosestValueInBst(Bst tree, int target) {
        return findClosestValueInBst(tree, target, tree.value);
    }

    public static int findClosestValueInBst(Bst tree, int target, int closest) {
        if (Math.abs(target - closest) > Math.abs(target - tree.value)) {
            closest = tree.value;
        }

        if (target < tree.value && tree.left != null) {
            return findClosestValueInBst(tree.left, target, closest);
        } else if (target > tree.value && tree.right != null) {
            return findClosestValueInBst(tree.right, target, closest);
        } else {
            return closest;
        }
    }

    public static void main(String[] args) {
        Bst root = new Bst(10);
        root.left = new Bst(5);
        root.left.left = new Bst(2);
        root.left.left.left = new Bst(1);
        root.left.right = new Bst(5);
        root.right = new Bst(15);
        root.right.left = new Bst(13);
        root.right.left.right = new Bst(14);
        root.right.right = new Bst(22);
        System.out.println(findClosestValueInBst(root, 12));
    }
}


package all.algos.binarySearchTrees;

public class Bst {

    public int value;
    public Bst left;
    public Bst right;

    public Bst(int value) {
        this.value = value;
    }
}

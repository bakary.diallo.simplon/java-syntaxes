package all.scanner;

import java.util.Scanner;

public class InoutHasNext {
    public static void main(String[] args) {
        int count = 0;
        Scanner input = new Scanner(System.in);
        System.out.print(" - : ");


        while (input.hasNext() && count < 4) {
            System.out.print(" - : ");
            System.out.println(input.next());
            System.out.print(" - : ");
            count++;
        }
    }
}

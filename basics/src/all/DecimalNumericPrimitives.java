package all;

public class DecimalNumericPrimitives {
    public static void main(String[] args) {
        float myNumber;
        myNumber = 10;
        System.out.println("myNumber= " + myNumber);

        double myDouble = 7.50;
        System.out.println("myDouble= " + myDouble);

        float myFloat1, myFloat2, myFloat3;
        float myFloat4;
        float myFloat5;

        float myFloat6 = 5f, myFloat7 = 10f, myFloat8;
        boolean b1, b2;
        myNumber = (float) 25.4F;
        myDouble = 2.54;
        double myDouble2 = 2.54F;
        double anotherDouble = 2.45D;

        double scientific = 5.000125E03;
        double scientific2 = 5.000125E3;
        double myDouble3 = 5000.125;

        System.out.println(scientific2);
    }
}

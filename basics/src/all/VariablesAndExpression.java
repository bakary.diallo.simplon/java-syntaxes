package all;

public class VariablesAndExpression {
    private static final int annual = 12;

    public static int getAnnualIncome(int salary) {
        return salary * annual;
    }

    public static void main(String[] args) {
        int dadIncome = getAnnualIncome(1200);
        int momIncome = getAnnualIncome(1200);
        int childIncome = getAnnualIncome(100 * 2);

        int familyIncome = dadIncome + momIncome + childIncome;
        System.out.println(familyIncome);
    }
}

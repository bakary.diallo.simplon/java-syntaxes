package all;


import all.methods.OverloadingMethods;

public class Test {
    public static void main(String[] args) {
        OverloadingMethods overloadingMethods = new OverloadingMethods();
        overloadingMethods.walk();
        overloadingMethods.walk(32768);
    }
}

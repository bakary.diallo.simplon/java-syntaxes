package all.staticImports;

import static all.staticImports.Config.*;
import static java.lang.Math.PI;
import static java.lang.Math.min;
import static java.lang.System.out;

public class ImportStatic {
    public static void main(String[] args) {
        int min = min(5, 7);
        out.println("min= " + min);
        out.println(PI);
        printConfig();
        out.println("name= " + name);
        out.println("column count= " + max_column_count);
    }
}

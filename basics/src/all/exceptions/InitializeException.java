package all.exceptions;

public class InitializeException extends Exception {
    public InitializeException(String message) {
        super(message);
    }
}

package all.exceptions;

public class CustomException extends Throwable {
    public int substract10FromLargeNumber(int number) throws Exception {
        if(number < 10){
            throw new InitializeException("The number pass was smaller than 10");
        }
        return number -10;
    }
}
